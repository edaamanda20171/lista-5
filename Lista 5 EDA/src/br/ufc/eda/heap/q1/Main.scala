package br.ufc.eda.heap.q1

object Main {
  def main(args: Array[String]): Unit = {

	println("HEAP COM VETOR");

	// item 1 - criando TAD Heap
	var obj: Object = new Heap();
	var heap: Heap = null;
	heap = (obj.asInstanceOf[Heap]).criarHeap(7);

	//item 2 - inserindo na heap
	heap.inserir(heap, 4);
	println(heap.prioridade);
	heap.inserir(heap, 1);
	println(heap.prioridade);
	heap.inserir(heap, 6);

	println(heap.prioridade);
	heap.inserir(heap, 3);
	println(heap.prioridade);
	heap.inserir(heap, 0);
	println(heap.prioridade);
	
	println(heap.prioridade);
	heap.inserir(heap, 2);
	println(heap.prioridade);
	heap.inserir(heap, 7);
	println(heap.prioridade);

	heap.inserir(heap, 8);
	
	//item 3 - buscando na heap
	println(heap.buscar(heap, 20));
	
	
	// item 4 - removendo da heap
	heap.remover(heap)
	println(heap.prioridade);
	heap.remover(heap)
	println(heap.prioridade);
	
	//item 5 - alterando elemento
	heap.alterar(heap, 2, 20)
	println(heap.prioridade);
	
	//item 6 - liberando a heap
	heap.liberarHeap(heap);

}
}